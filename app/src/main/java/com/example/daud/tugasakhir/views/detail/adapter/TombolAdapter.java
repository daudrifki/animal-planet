package com.example.daud.tugasakhir.views.detail.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.daud.tugasakhir.R;
import com.example.daud.tugasakhir.views.detail.OnStringChangedListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by riksa on 27/12/2017.
 */

public class TombolAdapter extends RecyclerView.Adapter<TombolAdapter.ViewHolder> {

    private List<String> list = new ArrayList<>();
    private List<String> soal = new ArrayList<>();
    private OnStringChangedListener mListener;

    private int positionnow = 0;
    private List<Integer> SequencePos = new ArrayList<>();

    public TombolAdapter(List<String> list, List<String> soal, OnStringChangedListener mListener) {
        this.list = list;
        this.soal = soal;
        this.mListener = mListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_tombol_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.btn.setText(list.get(position));

        boolean sama = false;
        for(int i = 0;i<SequencePos.size();i++){
            if(position == SequencePos.get(i)){
                sama = true;
            }
        }

        if(sama){
            holder.btn.setEnabled(false);
        }else{
            holder.btn.setEnabled(true);
        }

        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //jika posisi lebih dari soal maka tidak akan diproses
                if (positionnow == soal.size()) {
                    return;
                }

                holder.btn.setEnabled(false);
                positionnow++;
                mListener.onStringChanged(list.get(position));
                SequencePos.add(position);
            }
        });
    }

    public void backspace() {
        if(SequencePos.size() != 0) {
            positionnow--;
            SequencePos.remove(SequencePos.size() - 1);
            notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        Button btn;

        public ViewHolder(View itemView) {
            super(itemView);

            btn = (Button) itemView.findViewById(R.id.tombol_button);
        }
    }

}

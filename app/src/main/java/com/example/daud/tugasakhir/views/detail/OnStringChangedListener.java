package com.example.daud.tugasakhir.views.detail;

/**
 * Created by riksa on 27/12/2017.
 */

public interface OnStringChangedListener {
    void onStringChanged(String huruf);
}

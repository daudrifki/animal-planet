package com.example.daud.tugasakhir.views.detail;

import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.example.daud.tugasakhir.R;
import com.example.daud.tugasakhir.data.realmdb.LevelDaoRealm;
import com.example.daud.tugasakhir.databinding.DetailActivityBinding;
import com.example.daud.tugasakhir.views.detail.adapter.TombolAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;

public class DetailActivity extends AppCompatActivity implements OnStringChangedListener {

    DetailActivityBinding mBinding;

    private TombolAdapter adapter;
    private List<String> list;

    private String[] soal;

    private String nama, picture;

    private int level;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.detail_activity);
        mBinding = DataBindingUtil.setContentView(this, R.layout.detail_activity);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        realm = Realm.getDefaultInstance();

        //ambil data dari intent
        Bundle b = getIntent().getExtras();
        if(b != null) {
            nama = b.getString("nama").toUpperCase();
            picture = b.getString("picture");
            level = b.getInt("level");
        }

        int resid = getResources().getIdentifier(picture, "drawable", getPackageName());

        mBinding.detailIv.setImageResource(resid);

        soal = nama.split("");
        String[] a = soal;

        list = new ArrayList<>(Arrays.asList(a));
        List<String> listsoal = new ArrayList<>(Arrays.asList(a));

        for(int i = 0;i<list.size();i++){
            if(list.get(i).equals("") || list.get(i).equals(" ")){
                list.remove(i);
                listsoal.remove(i);
            }
        }

        Collections.shuffle(list);

        adapter = new TombolAdapter(list, listsoal, this);

        mBinding.detailRv.setAdapter(adapter);
        mBinding.detailRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false));
        isNextAvailable();

        mBinding.detailBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realm.beginTransaction();
                LevelDaoRealm dao = realm.where(LevelDaoRealm.class).equalTo("level", level+1).findFirst();
                dao.setLocked(false);
                realm.commitTransaction();
                realm.close();
                finish();
            }
        });

        //delete button
        mBinding.detailBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.backspace();
                if(mBinding.detailTv.getText().toString().length() != 0) {
                    mBinding.detailTv.setText(mBinding.detailTv.getText().toString().substring(0, mBinding.detailTv.getText().toString().length() - 1));
                }
            }
        });
    }

    @Override
    public void onStringChanged(String huruf) {
        mBinding.detailTv.append(huruf);
        isNextAvailable();
    }


    private void isNextAvailable(){
        StringBuilder builder = new StringBuilder();
        for (String value : soal) {
            builder.append(value);
        }
        String soaljadi = builder.toString();

        if(mBinding.detailTv.getText().toString().equals(soaljadi)){
            mBinding.detailBtnNext.setEnabled(true);
        }
    }
}

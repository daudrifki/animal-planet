package com.example.daud.tugasakhir.data.realmdb;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by riksa on 28/12/2017.
 */

public class LevelDaoRealm extends RealmObject {
    @PrimaryKey
    private int level;
    private String picture;
    private String nama;
    private boolean locked;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}

package com.example.daud.tugasakhir.views.level.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.daud.tugasakhir.R;
import com.example.daud.tugasakhir.data.realmdb.LevelDaoRealm;
import com.example.daud.tugasakhir.views.detail.DetailActivity;
import com.example.daud.tugasakhir.views.detail.OnStringChangedListener;
import com.example.daud.tugasakhir.views.level.adapter.dao.LevelDao;

import java.util.List;

/**
 * Created by riksa on 27/12/2017.
 */

public class LevelAdapter extends RecyclerView.Adapter<LevelAdapter.ViewHolder>{

    private List<LevelDaoRealm> list;

    public LevelAdapter(List<LevelDaoRealm> list) {
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.level_item, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.btn.setText(list.get(position).getLevel()+"");

        if(list.get(position).isLocked()) {
            //TODO : Implemen jika button level masih dikunci
            holder.btn.setEnabled(false);
        }else{
            holder.btn.setEnabled(true);
        }

        holder.btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Jika button level sudah dibuka
                Intent i = new Intent(holder.itemView.getContext(), DetailActivity.class);
                //TODO : Implemen put extra untuk set data ke detail
                i.putExtra("nama", list.get(position).getNama());
                i.putExtra("picture", list.get(position).getPicture());
                i.putExtra("level", list.get(position).getLevel());
                holder.itemView.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setList(List<LevelDaoRealm> list){
        this.list = list;
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        Button btn;

        private ViewHolder(View itemView) {
            super(itemView);

            btn = (Button) itemView.findViewById(R.id.level_btn);
        }
    }
}

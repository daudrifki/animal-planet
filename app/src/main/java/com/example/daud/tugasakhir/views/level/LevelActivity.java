package com.example.daud.tugasakhir.views.level;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.WindowManager;

import com.example.daud.tugasakhir.R;
import com.example.daud.tugasakhir.data.realmdb.LevelDaoRealm;
import com.example.daud.tugasakhir.databinding.LevelActivityBinding;
import com.example.daud.tugasakhir.views.level.adapter.LevelAdapter;
import com.example.daud.tugasakhir.views.level.adapter.dao.LevelDao;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by riksa on 27/12/2017.
 */

public class LevelActivity extends AppCompatActivity {

    LevelActivityBinding mBinding;

    private LevelAdapter adapter;
    private List<LevelDaoRealm> list = new ArrayList<>();

    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.level_activity);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        realm = Realm.getDefaultInstance();

        adapter = new LevelAdapter(list);
        mBinding.levelRv.setAdapter(adapter);
        mBinding.levelRv.setLayoutManager(new GridLayoutManager(this, 4));

        getAllLevel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        getAllLevel();
    }

    private void getAllLevel(){
        RealmResults<LevelDaoRealm> daolist = realm.where(LevelDaoRealm.class).findAll();
        adapter.setList(daolist);
        adapter.notifyDataSetChanged();
    }
}

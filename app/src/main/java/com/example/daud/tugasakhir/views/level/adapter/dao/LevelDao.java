package com.example.daud.tugasakhir.views.level.adapter.dao;

/**
 * Created by riksa on 27/12/2017.
 */

public class LevelDao {
    private String level;
    private boolean locked;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}

package com.example.daud.tugasakhir;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by riksa on 28/12/2017.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}

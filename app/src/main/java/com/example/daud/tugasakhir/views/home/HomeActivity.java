package com.example.daud.tugasakhir.views.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.daud.tugasakhir.R;
import com.example.daud.tugasakhir.data.realmdb.LevelDaoRealm;
import com.example.daud.tugasakhir.databinding.HomeActivityBinding;
import com.example.daud.tugasakhir.views.level.LevelActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;

import io.realm.Realm;

public class HomeActivity extends AppCompatActivity {

    private HomeActivityBinding mBinding;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        setContentView(R.layout.home_activity);
        mBinding = DataBindingUtil.setContentView(this, R.layout.home_activity);

        realm = Realm.getDefaultInstance();

        mBinding.homeBtnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, LevelActivity.class));
            }
        });

        //TODO : Implemen pertama kali buka app
        SharedPreferences sp = getSharedPreferences("pref", MODE_PRIVATE);
        if(sp.getBoolean("firsttime", true)){
            loadJson();
            SharedPreferences.Editor editor = sp.edit();
            editor.putBoolean("firsttime", false);
            editor.apply();
        }
    }

    private void loadJson(){
        String json;
        try{
            InputStream is = getAssets().open("mock_data.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            json = new String(buffer, "UTF-8");
            JSONArray jsonArray = new JSONArray(json);

            for(int i = 0;i<jsonArray.length();i++){
                JSONObject obj = jsonArray.getJSONObject(i);

                realm.beginTransaction();

                LevelDaoRealm dao = realm.createObject(LevelDaoRealm.class, obj.getInt("level"));
                dao.setNama(obj.getString("nama"));
                dao.setPicture(obj.getString("picture"));
                if(obj.getInt("level") == 1){
                    dao.setLocked(false);
                }else {
                    dao.setLocked(true);
                }
                realm.commitTransaction();
            }

        }catch (Exception ex){
            ex.printStackTrace();
        }

        realm.close();
    }
}
